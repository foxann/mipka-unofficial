package ru.nonoffical.mipka;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    private WebView mipedviewer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mipedviewer = (WebView)findViewById(R.id.mipedviewer);

        WebSettings webSettings = mipedviewer.getSettings();
        webSettings.setSavePassword(true);
        webSettings.setSaveFormData(true);
        webSettings.setJavaScriptEnabled(true);

        mipedviewer.setWebViewClient(new mipedclient());
        mipedviewer.loadUrl("http://miped.ru/f/");
    }


    private class mipedclient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.indexOf("miped.ru/f") <= 0) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.getContext().startActivity(intent);
                return true;
            }
            return false;
        }
    }


    @Override
    public void onBackPressed() {
        if(mipedviewer.canGoBack()) {
            mipedviewer.goBack();
        } else {
            super.onBackPressed();
        }
    }
}